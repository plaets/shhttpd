#include <iostream>
#include <string>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <thread>
#include <chrono>
#include <vector>
#include <signal.h>
#include <mutex>
#include <functional>
#include <condition_variable>
#include <errno.h>
#include <map>
#include <utility>
#include <filesystem>
#include <fstream>
using namespace std::chrono_literals;

bool quit = false;

std::vector<std::string> split(std::string data, std::string delimeter, int maxElements = -1){ //should use uniq_ptr
    std::vector<std::string> lines;
    size_t lastPos = 0;
    while(maxElements != -1 ? --maxElements >= 0 : true){ //not good?
        auto newPos = data.find(delimeter, lastPos);
        lines.push_back(data.substr(lastPos, newPos-lastPos));
        if(newPos == std::string::npos) break;
        lastPos = newPos + delimeter.length();
    }
    return lines;
}

struct HTTPResponse{
    std::string status = "200";
    std::string description;
    std::string httpVersion = "HTTP/1.1";
    std::string data;
    std::map<std::string, std::string> headers;

    HTTPResponse(){
    }

    HTTPResponse(std::string data){
        parseResponse(data);
    }

    std::string toString(bool endHeaders=true){  //should return uniq_ptr
        std::string data;
        data += httpVersion + " "; 
        data += status + " ";
        data += description + "\r\n";
        data += stringifyHeaders() + (endHeaders ? "\r\n" : "");
        data += this->data;
        return data;
    }

    std::string stringifyHeaders(){ //should return uniq_ptr
        std::string data;
        for(auto& n : headers)
            data += n.first + ": " + n.second + "\r\n";
        return data;
    }

private:
    void parseHeaders(const std::vector<std::string>& lines){
        for(size_t i = 1; i != lines.size()-1; i++){
            auto headerLine = split(lines[i], ": ", 2);
            if(lines[i] != "\r\n" && headerLine.size() == 2)
                headers[headerLine[0]] = headerLine[1];
                //headers.insert(std::make_pair(headerLine[0], headerLine[1])); //not good
            else break;
        }
    }

    void parseFirstLine(const std::string& data){
        auto firstLine = split(data, " ", 3);
        httpVersion = firstLine[0];
        status = firstLine[1];
        description = firstLine[2];
    }

    void parseResponse(std::string& data){
        auto lines = split(data, "\r\n");
        parseFirstLine(lines[0]);
        parseHeaders(lines);
    }
};

struct HTTPRequest{
    std::string method;
    std::string pathString;
    std::string path;
    std::string queryString;
    std::string httpVersion;
    std::string data;
    std::string args;
    std::map<std::string, std::string> headers;
    std::map<std::string, std::string> query;

    HTTPRequest(std::string data){
        parseRequest(data);
    }

    std::string toString(bool endHeaders=true){  //should return uniq_ptr
        std::string data;
        data += method + " "; 
        data += (path == "" ? stringifyPath() : path) + " ";
        data += httpVersion + "\r\n";
        data += stringifyHeaders() + (endHeaders ? "\r\n" : "");
        data += this->data;
        return data;
    }

    std::string stringifyHeaders(){ //should return uniq_ptr
        std::string data;
        for(auto& n : headers)
            data += n.first + ": " + n.second + "\r\n";
        return data;
    }

    std::string stringifyPath(){ //should return uniq_ptr
        std::string data;
        data += path;

        if(query.size() > 0) data += "?";
        for(auto& n : query)
            if(n.second != "") data += n.first + "=" + n.second + "&";
            else data += n.first + "&";

        if(query.size() > 0) return data.substr(0, data.size()-1);
        else return data;
    }

private:
    void parseHeaders(const std::vector<std::string>& lines){
        if(lines.size() <= 1) return;

        for(size_t i = 1; i != lines.size()-1; i++){
            auto headerLine = split(lines[i], ": ", 2);
            if(lines[i] != "\r\n" && headerLine.size() == 2)
                headers[headerLine[0]] = headerLine[1]; 
                //headers.insert(std::make_pair(headerLine[0], headerLine[1])); //not good
            else break;
        }
    }

    void parseFirstLine(const std::string& data){
        auto firstLine = split(data, " ");
        method = firstLine[0];
        pathString = firstLine.size() > 1 ? firstLine[1] : "";
        httpVersion = firstLine.size() > 2 ? firstLine[2] : "";
    }

    void parseRequest(std::string& data){
        auto lines = split(data, "\r\n");
        parseHeaders(lines);
        parseFirstLine(lines[0]);
        parseQueryString(pathString);
    }

    void parseQueryString(const std::string& pathString){
        auto data = split(pathString, "?", 2);
        path = data[0];
        if(data.size() > 1){
            queryString = data[1];
            parseQuery(queryString);
        }
    }

    void parseQuery(const std::string queryString){
        auto queryParts = split(queryString, "&"); 
        for(auto& n : queryParts){
            auto tmp = split(n, "=");
            query[tmp[0]] = tmp[1];
            //query.insert(std::make_pair(tmp[0], tmp.size() > 1 ? tmp[1] : ""));
        }
    }
};

class ThreadPool{ //inspired by https://github.com/progschj/ThreadPool and https://github.com/KrzaQ/monalisa-cpp/blob/master/main.cpp
    std::vector<std::thread> threads; 
    std::vector<std::function<void()>> tasks;
    std::mutex tasks_mutex;
    std::condition_variable cv;
    bool stop = false;

    void work(){
        while(true){
            std::function<void()> task;
            {
                std::unique_lock<std::mutex> lock(tasks_mutex);
                if(tasks.empty()) cv.wait(lock);
                if(stop && tasks.empty()) return;

                if(!tasks.empty()){
                    task = tasks.back();
                    tasks.pop_back();
                }
            }
            if(task) task();
        }
    }

public:
    ThreadPool(size_t numOfThreads){
        for(size_t i = 0; i != numOfThreads; i++)
            threads.push_back(std::thread([this](){ work(); }));
    }

    void push(std::function<void()> task){
        {
            std::lock_guard<std::mutex> guard(tasks_mutex);
            tasks.push_back(task);
        }
        cv.notify_one();
    }

    ~ThreadPool(){
        {
            stop = true;
            std::unique_lock<std::mutex> lock(tasks_mutex);
        }

        cv.notify_all();
        for(auto &n : threads) n.join();
    }
};

class TCPSocketException: public std::system_error{ using std::system_error::system_error;
};

class TCPConnection{
    int connection;
    unsigned int bufferSize = 256;

public:
    TCPConnection(int connection){
        this->connection = connection;
    }

    int sendData(const std::string &msg){
        int tries = 0;
        int return_val;

        while(tries++ < 5){
            return_val = send(this->connection, msg.c_str(), msg.length(), 0);
            if(return_val >= 0) break;
        }

        if(return_val < 0)
            throw TCPSocketException(errno, std::generic_category(), "Could not send data"); 

        return return_val;
    } 

    std::string reciveData(unsigned int maxData = -1){ //should use unique pointer
        std::string data;

        while(data.length() < maxData){
            char buffer[bufferSize];
            auto toRecive = maxData - data.size();
            auto bytes = recv(connection, &buffer, toRecive > bufferSize ? bufferSize : toRecive, 0);
            data.append(buffer, bytes);
            if(bytes < bufferSize) break;
        }

        return data;
    }

    std::string reciveUntil(const std::string& signature, unsigned int maxData = -1){
        std::string data;
        while(data.length() < maxData){
            auto recivedData = reciveData(bufferSize);
            auto pos = recivedData.find(signature);
            data.append(recivedData.substr(0, pos));
            if(pos != std::string::npos) break;
        }
        return data;
    }

    ~TCPConnection(){
        if(close(connection) < 0){
            TCPSocketException exception(errno, std::generic_category(), "Could not close connection");
            std::cout<<exception.what()<<" "<<exception.code()<<std::endl;
        }
    }
};

class TCPSocket{
    int socket_fd; 
    sockaddr_in address;
    socklen_t addr_len; 

public:
    TCPSocket(){
        socket_fd = socket(AF_INET, SOCK_STREAM, 0);

        if(socket_fd < 0)
            throw TCPSocketException(errno, std::generic_category(), "Could not create socket"); 

        int opt = 1;
        if(setsockopt(socket_fd , SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt))  < 0)
            throw TCPSocketException(errno, std::generic_category(), "Could not set socket options"); 
    }

    TCPSocket(unsigned int port): TCPSocket(){
        initSocket(port);
    }

    void initSocket(unsigned int port){
        address.sin_family = AF_INET;
        address.sin_addr.s_addr = INADDR_ANY;
        address.sin_port = htons(port);
        addr_len = sizeof(address); 

        if(bind(socket_fd, (const sockaddr*)&address, sizeof(address)) < 0)
            throw TCPSocketException(errno, std::generic_category(), "Could not bind socket"); 

        if(listen(socket_fd, 3) < 0)
            throw TCPSocketException(errno, std::generic_category(), "Could not start listening"); 
    }

    TCPConnection acceptConnection(){
        return TCPConnection(acceptConnectionRaw());
    }

    int acceptConnectionRaw(){
        auto connection = accept(socket_fd, (sockaddr*)&address, &addr_len);

        if(connection < 0)
            throw TCPSocketException(errno, std::generic_category(), "Could not accept connection"); 

        return connection;
    }
};

class SignalException{
    int sig_num;
public:
    SignalException(int sig_num){
        this->sig_num = sig_num;
    }

    int getSigNum(){
        return sig_num;
    }
};

int main(){
    ThreadPool thread_pool(64); 
    TCPSocket socket(8080);

    signal(SIGPIPE, [](int){
        std::cout<<"Caught a SIGPIPE\n";
    });

    try{
        signal(SIGINT, [](int signum){
            std::cout<<"Caught a SIGINT\n"; 
            throw SignalException(signum); 
        });

        while(!quit){
            int connection = socket.acceptConnectionRaw();
            thread_pool.push([connection](){
                auto tcpConnection = TCPConnection(connection);
                std::string msg;
                auto data = tcpConnection.reciveData(1024*8);
                HTTPRequest req(data);

                auto path = req.path.size() > 2 ? split(req.path.substr(1, req.path.size()-1), "../").back() : req.path;
                bool error = false;

                std::filesystem::path fspath;

                try{
                    fspath = std::filesystem::path(std::filesystem::canonical(std::filesystem::path(path)));
                }catch(std::exception e){
                    error = true;
                }

                std::cout<<fspath<<"\n";

                HTTPResponse resp;
                if(!error && std::filesystem::exists(fspath) && std::filesystem::is_regular_file(fspath)){
                    struct stat s;
                    stat(fspath.string().c_str(), &s);

                    //stolen from https://stackoverflow.com/questions/478898/how-do-i-execute-a-command-and-get-output-of-command-within-c-using-posix 
                    //and https://stackoverflow.com/questions/13098620/using-stat-to-check-if-a-file-is-executable-in-c
                    if(s.st_mode & S_IXUSR){ 
                        std::array<char, 256> buffer;

                        setenv("REQ_METHOD", req.method.c_str(), true);
                        setenv("REQ_PATH", req.path.c_str(), true);
                        setenv("REQ_QUERY", req.queryString.c_str(), true);
                        setenv("REQ_DATA", req.data.c_str(), true);
                        setenv("REQ_HEADERS", req.stringifyHeaders().c_str(), true);
                        std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(fspath.string().c_str(), "r"), pclose); //the thing with deleter is *really cool*

                        try{
                            tcpConnection.sendData(resp.toString(false));
                            if(!pipe) throw std::runtime_error("popen failed");
                            while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) tcpConnection.sendData(buffer.data());
                        }catch(TCPSocketException& e){
                            std::cout<<"Error while sending data: "<<e.what()<<std::endl;
                        }
                    }else{
                        std::ifstream ifs(path, std::ifstream::in);
                        std::string response_data((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
                        resp.data = response_data;
                        tcpConnection.sendData(resp.toString());
                    }
                }else{
                    resp.data = "<h1>404</h1>";
                    tcpConnection.sendData(resp.toString());
                }
            });
        }
    }catch(SignalException& e){
        std::cout<<"Joining threads...\n"; 
    }
}
