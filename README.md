# Simple HTTP Server

## How to run

`g++ --std=c++17 shhttp.cpp  -fext-numeric-literals -lpthread -lstdc++fs -o shhttpd`

After compilation, shhttpd will be the resulting executable file.
The server will run on port 8080 (no configuration implemented yet) and serve files from its working directory.